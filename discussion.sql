-- Add New Records
-- Add 5 artist, 2 albums each, 2 songs per album

-- Artist
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Fearless", "2008-1-1", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lovestory", 213, "Country rock", 6);

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Red", "2012-1-1", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 243, "Rock, alternative rock", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 7);


-- Lady Gaga
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("A Star is Born", "2018-1-1", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black eyes", 141, "Rock and roll", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Countr, rock, folk rock", 8);

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Born This Way", "2011-1-1", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Marry The Night", 201, "Electropop", 9);


-- Justin Bieber
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Purpose", "2012-1-1", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 152, "Dancehall-poptropical housemoombahton", 10);

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Believe", "2012-1-1", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 11);


-- Ariana Grande
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Dangerous Woman", "2016-1-1", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into you", 242, "EDM house", 11);

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Thank U, Next", "2019-1-1", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U, Next", 146, "Pop, R&B", 12);


-- Bruno Mars
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("24k Magic", "2016-1-1", 10);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 204, "Funk, disco, R&B", 14);

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Earth to Mars", "2011-1-1", 10);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 132, "Pop", 15);


-- [SECTION] Advanced Selects

-- Exclude reecords
SELECT * FROM songs WHERE id != 11;
SELECT * FROM songs WHERE genre != "K-pop";
SELECT * FROM songs WHERE album_id != 5;

-- Greater than or equal
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE length > 250;
-- Get specific songs through ID (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5

-- IN (include)
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");
SELECT * FROM songs WHERE genre IN ("Pop", "R&B");

-- Combine conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%a"; --select keyword from the end
SELECT * FROM songs WHERE song_name LIKE "a%"; --select keyword from the start
SELECT * FROM songs WHERE song_name LIKE "%a%"; --select keyword that has the keyword
SELECT * FROM albums WHERE data_released LIKE "201_______";
SELECT * FROM albums WHERE data_released LIKE "________01";
SELECT * FROM albums WHERE album_title LIKE "Pur_ose";


-- Sorting records
SELECT * FROM songs ORDER BY song_name DESC;
SELECT * FROM songs ORDER BY song_name ASC;

-- Getting distinct records
SELECT DISTINCT genre FROM songs;

-- Getting sensitive query
SELECT * FROM songs WHERE genre = BINARY "Pop";
SELECT * FROM songs WHERE song_name LIKE BINARY "B%";


-- [SECTION] Joining Tables

-- Combinee artist and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;